<?php

use Magento\Framework\Component\ComponentRegistrar;
/**
 * Copyright © Kowal. All rights reserved.
 * See LICENSE.txt for license details.
 */
ComponentRegistrar::register(
    ComponentRegistrar::MODULE,
    'Kowal_CatalogHoverImage',
    __DIR__
);
