<?php

namespace Kowal\CatalogHoverImage\Block;

use Magento\Catalog\Block\Product\Context;
use Magento\Framework\Data\Helper\PostHelper;
use Magento\Catalog\Model\Layer\Resolver;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Framework\Url\Helper\Data;
use Magento\Catalog\Api\CategoryRepositoryInterface;
use Magento\Catalog\Block\Product\ListProduct;
use Kowal\CatalogHoverImage\Helper\Config as HelperConfig;
use Magento\Catalog\Helper\Image as ImageHelper;

class HoverList extends ListProduct
{
    /**
     * @var HelperConfig
     */
    private $helperConfig;

    /**
     * @var Json
     */
    private $serializer;

    /**
     * @var ImageHelper
     */
    private $imageHelper;

    /**
     * @param Context $context
     * @param PostHelper $postDataHelper
     * @param Resolver $layerResolver
     * @param CategoryRepositoryInterface $categoryRepository
     * @param Data $urlHelper
     * @param HelperConfig $helperConfig
     * @param Json $serializer
     * @param ImageHelper $imageHelper
     * @param array $data
     */
    public function __construct(
        Context                     $context,
        PostHelper                  $postDataHelper,
        Resolver                    $layerResolver,
        CategoryRepositoryInterface $categoryRepository,
        Data                        $urlHelper,
        HelperConfig                $helperConfig,
        Json                        $serializer,
        ImageHelper                 $imageHelper,
        array                       $data = []
    )
    {
        $this->helperConfig = $helperConfig;
        $this->serializer = $serializer;
        $this->imageHelper = $imageHelper;
        parent::__construct(
            $context,
            $postDataHelper,
            $layerResolver,
            $categoryRepository,
            $urlHelper,
            $data
        );
    }

    /**
     * @return string
     */
    public function getHoverImageList()
    {
        $hoverImages = [];
        $productCollection = $this->getLoadedProductCollection();
        foreach ($productCollection as $product) {
            $hoverCatalog = $product->getData('hover_catalog');
            $thumbnail = $product->getData('thumbnail');

            if (!empty($hoverCatalog) && $hoverCatalog != 'no_selection') {
                $hoverImages[$product->getData('entity_id')] = $this->getImageUrl($product, 'hover_image');
            } elseif (!empty($thumbnail) && $thumbnail != 'no_selection') {
                $hoverImages[$product->getData('entity_id')] = $this->getImageUrl($product, 'hover_image_thumbnail');
            } else {
                $hoverImages[$product->getData('entity_id')] = false;
            }
        }

        return $this->serializer->serialize($hoverImages);
    }

    /**
     * Pobiera URL obrazu produktu.
     *
     * @param \Magento\Catalog\Model\Product $product
     * @param string $imageId
     * @return string
     */
    private function getImageUrl($product, $imageId)
    {
        return $this->imageHelper->init($product, $imageId)->getUrl();
    }

    /**
     * @return bool
     */
    public function isEnabled()
    {
        return $this->helperConfig->isEnabled();
    }
}