<?php

namespace Kowal\CatalogHoverImage\Block;

use Magento\Framework\View\Element\Template\Context;
use Kowal\CatalogHoverImage\Helper\Config as HelperConfig;

class DefaultHoverList extends \Magento\Framework\View\Element\Template
{
    /**
     * @var HelperConfig
     */
    private $helperConfig;

    /**
     * @param Context $context
     * @param HelperConfig $helperConfig
     * @param array $data
     */
    public function __construct(
        Context      $context,
        HelperConfig $helperConfig,
        array        $data = []
    )
    {
        $this->helperConfig = $helperConfig;
        parent::__construct(
            $context,
            $data
        );
    }

    /**
     * @return string
     */
    public function getHoverImageList()
    {
        return '{}';
    }

    /**
     * @return bool
     */
    public function isEnabled()
    {
        return $this->helperConfig->isEnabled();
    }
}
