/**
* Copyright © Kowal. All rights reserved.
* See LICENSE.txt for license details.
*/

define([
    'underscore',
    'jquery',
    'jquery/ui'
], function (_, $) {

    $.widget('kowal.hoverList', {
        options: {
            hoverData: {},
            origData: {},
            initializedItems: {},
            itemsSelector: ".product-item",
            itemImageSelector: "product-image-photo",
            hoverInitializedSelector: "hoverInited"
        },

        _create: function () {
            let self = this;
            this.update();
            $(document).on('contentUpdated', 'body', function () {
                self.update();
            });
        },

        _bind: function (elements) {
            let self = this;
            elements = elements || $(this.options.itemsSelector).not('.' + self.options.hoverInitializedClass);
            elements
                .on('mouseover', function (event) {
                    self.onMouseOver(event)
                })
                .on('mouseout', function (event) {
                    self.onMouseOut(event)
                });
        },

        onMouseOver: function (event) {
            let $elementTarget = $(event.target);
            if (!$elementTarget.hasClass(this.options.itemImageSelector)) {
                return;
            }
            let $element = $(event.currentTarget);
            let productId = $('.price-box', $element).attr('data-product-id');
            if (this.options.hoverData[productId]) {
                let $image = $('.'+this.options.itemImageSelector, $element);
                this.options.origData[productId] = $image.attr('src');
                $image.attr('src', this.options.hoverData[productId]);
            }
        },

        onMouseOut: function (event) {
            let $element = $(event.currentTarget);
            let productId = $('.price-box', $element).attr('data-product-id');
            if (this.options.origData[productId]) {
                let $image = $('.'+this.options.itemImageSelector, $element);
                $image.attr('src', this.options.origData[productId]);
            }
        },

        update: function () {
            let self = this;
            let ids = [];
            $(self.options.itemsSelector).not('.' + self.options.hoverInitializedClass).each(function() {
                let $element = $(this);
                $element.addClass(self.options.hoverInitializedClass);
                let imageContainer = $element.find('.' + self.options.itemImageSelector);
                if (imageContainer.length > 0) {
                    let productId = $('.price-box', $element).attr('data-product-id');
                    if (productId) {
                        if (!self.options.hoverData.hasOwnProperty(productId)) {
                            self.options.hoverData[productId] = false;
                            ids.push(productId);
                        }
                        if (!self.options.initializedItems.hasOwnProperty(productId)) {
                            self.options.initializedItems[productId] = true;
                            self._bind($element);
                        }
                    }
                }
            });

            if (ids.length > 0) {
                this.requestData(ids);
            }
        },

        requestData: function (ids) {
            let self = this;
            return $.ajax({
                url: this.options.updateUrl,
                type: 'GET',
                data: {ids: ids},
                contentType: 'application/json'
            }).done(function (response) {
                if (response.images) {
                    _.each(response.images, function (url, productId) {
                        self.options.hoverData[productId] = url;
                    });
                }
            });
        }
    });

    return $.kowal.hoverList;

});

