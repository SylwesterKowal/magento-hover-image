<?php

namespace Kowal\CatalogHoverImage\Controller\Images;

use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use Magento\Catalog\Helper\Image as ImageHelper;

class Update extends \Magento\Framework\App\Action\Action
{
    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @var CollectionFactory
     */
    protected $productCollectionFactory;

    /**
     * @var ImageHelper
     */
    protected $imageHelper;

    /**
     * @var Json
     */
    protected $serializer;

    /**
     * @param Context $context
     * @param ScopeConfigInterface $scopeConfig
     * @param CollectionFactory $productCollectionFactory
     * @param ImageHelper $imageHelper
     * @param Json $serializer
     */
    public function __construct(
        Context              $context,
        ScopeConfigInterface $scopeConfig,
        CollectionFactory    $productCollectionFactory,
        ImageHelper          $imageHelper,
        Json                 $serializer
    )
    {
        $this->scopeConfig = $scopeConfig;
        $this->productCollectionFactory = $productCollectionFactory;
        $this->imageHelper = $imageHelper;
        $this->serializer = $serializer;

        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\Controller\Result\Json
     */
    public function execute()
    {
        $images = [];
        $ids = $this->getRequest()->getParam('ids', []);
        if (!empty($ids)) {
            /** @var \Magento\Catalog\Model\ResourceModel\Product\Collection $collection */
            $collection = $this->productCollectionFactory->create();
            $collection->addFieldToFilter('entity_id', ['in' => $ids])
                ->addAttributeToSelect(['hover_catalog', 'thumbnail']);

            foreach ($collection->getItems() as $product) {

                if (!empty($product->getData('hover_catalog'))
                    && $product->getData('hover_catalog') != 'no_selection'
                ) {
                    $images[$product->getData('entity_id')] = $this->getImage($product, 'hover_image');
                } else if (!empty($product->getData('thumbnail'))
                    && $product->getData('thumbnail') != 'no_selection') {
                    $images[$product->getData('entity_id')] = $this->getImage($product, 'product_thumbnail_image');
                } else {
                    $images[$product->getData('entity_id')] = false;
                }
            }
        }
        return $this->resultFactory
            ->create(\Magento\Framework\Controller\ResultFactory::TYPE_JSON)
            ->setData(['images' => $images]);
    }

    /**
     * @param $product
     * @param $imageId
     * @return string
     */
    protected function getImage($product, $imageId)
    {
        return $this->imageHelper->init($product, $imageId)->getUrl();
    }
}